function searchAnswer() {
    let keyword = $("#askQuestion").val();
    console.log(keyword);
    let url = baseUrl + "/search-all-post";
    $.post(url, {keyword})
        .done(function (response) {
            let success = response.success;
            let data = response.body;
            let firstTimeShowInstance = $(".firstTimeShow");
            let html = "";
            if (success) {
                $.each(data, function (index, post) {
                    let title = post.title;
                    let blogUrl = post.blogUrl;
                    let youtubeUrl = post.youtubeUrl;
                    let image = post.image;
                    let imageUrl = "";
                    let youtubeText = "";
                    if (image != null && image !== "") {
                        imageUrl = `<div style="text-align: center">
                              <a href="${baseUrl}/files/post/${image}" target="_blank">
                                  <image src="${baseUrl}/files/post/${image}" width="250" height="150"/></a>
                          </div><br>`;
                    }

                    if (youtubeUrl != null && youtubeUrl !== "") {
                        youtubeText = `<div style="text-align: center">
                             <iframe width="500" height="400"
                                        src="https://www.youtube.com/embed/${youtubeUrl}">
                                </iframe>
                          </div><br>`;
                    }

                    html += `<div style="border-width: thin !important;">
                                <h1 style="text-align: center">${index + 1}. ${title}</h1>`
                    if (blogUrl != null && blogUrl !== "") {
                        html += `<h5 style="text-align: center;"><a href="${post.blogUrl}" target="_blank">${post.blogUrl}</a></h5>`;
                    }

                    html += imageUrl;
                    html += youtubeText;
                    html += `<br><br>
                                <div class="description" style="text-align: center;">
                                        ${post.description}
                                </div>
                            </div>
                            <br>`;
                });
                let dynamicContainerInstance = $(".dynamicContainer");
                dynamicContainerInstance.show();
                dynamicContainerInstance.html(html);
                firstTimeShowInstance.hide()
            } else {
                alert("Sorry we can't find any answer for your question.");
                // firstTimeShowInstance.show();
            }
        })
        .fail(function () {
            alert("Something went wrong.");
        })
        .always(function () {

        });
}