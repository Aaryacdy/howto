<%--
  Created by Aarya
  User: aarya
  Date: 2/23/2022
  Time: 10:53 PM
  To change this template use File | Settings | File Templates.
--%>

<footer class="app-footer">
    <div class="container text-center py-3">
        <!--/* This template is free as long as you keep the footer attribution link. If you'd like to use the template without the attribution link, you can buy the commercial license via our website: themes.3rdwavemedia.com Thank you for your support. :) */-->
        <small class="copyright"><i class="fas fa-heart" style="color: #fb866a;"></i><a class="app-link"
                                                                                        href="https://kathford.edu.np"
                                                                                        target="_blank">Kathford
            International College</a></small>

    </div>
</footer><!--//app-footer-->

</div>
<!--//app-wrapper-->

<!-- Javascript -->
<script src="${baseUrl}/resources/bootstrap5/plugins/popper.min.js"></script>
<script src="${baseUrl}/resources/bootstrap5/plugins/bootstrap/js/bootstrap.min.js"></script>

<!-- Charts JS -->
<script src="${baseUrl}/resources/bootstrap5/plugins/chart.js/chart.min.js"></script>
<script src="${baseUrl}/resources/bootstrap5/js/index-charts.js"></script>
<script src="${baseUrl}/resources/other/login/vendor/jquery/jquery-3.2.1.min.js"></script>

<!-- Page Specific JS -->
<script src="${baseUrl}/resources/bootstrap5/js/app.js"></script>

</body>
</html>
