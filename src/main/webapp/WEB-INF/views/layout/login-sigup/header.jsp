<%--
  Created by Aarya Chaudhary
  User: aarya
  Date: 1/16/2022
  Time: 11:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%--<c:set var="baseUrl" value="${pageContext.request.contextPath}"/>--%>
<!--===============================================================================================-->
<link rel="icon" type="image/png" href="${baseUrl}/resources/other/login/images/icons/favicon.ico"/>
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
      href="${baseUrl}/resources/other/login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
      href="${baseUrl}/resources/other/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
      href="${baseUrl}/resources/other/login/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="${baseUrl}/resources/other/login/vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
      href="${baseUrl}/resources/other/login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
      href="${baseUrl}/resources/other/login/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="${baseUrl}/resources/other/login/vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href=${baseUrl}/resources/other/login/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="${baseUrl}/resources/other/login/css/util.css">
<link rel="stylesheet" type="text/css" href="${baseUrl}/resources/other/login/css/main.css">
<!--===============================================================================================-->
