<%--
  Created by Aarya Chaudhary
  User: aarya
  Date: 1/16/2022
  Time: 11:29 PM
  To change this template use File | Settings | File Templates.
--%>

<c:set var="baseUrl" value="${pageContext.request.contextPath}"/>
<!--===============================================================================================-->
<script src="${baseUrl}/resources/other/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="${baseUrl}/resources/other/login/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="${baseUrl}/resources/other/login/vendor/bootstrap/js/popper.js"></script>
<script src="${baseUrl}/resources/other/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="${baseUrl}/resources/other/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="${baseUrl}/resources/other/login/vendor/daterangepicker/moment.min.js"></script>
<script src=${baseUrl}/resources/other/login/"vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="${baseUrl}/resources/other/login/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="${baseUrl}/resources/other/login/js/main.js"></script>

