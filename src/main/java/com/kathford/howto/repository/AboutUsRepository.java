package com.kathford.howto.repository;

import com.kathford.howto.model.AboutUs;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AboutUsRepository extends JpaRepository<AboutUs, Long> {
}
