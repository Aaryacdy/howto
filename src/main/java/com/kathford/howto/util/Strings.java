package com.kathford.howto.util;

public class Strings {

    public static boolean IS_NEW_DESIGN = true;

    public static final String SOMETHING_WENT_WRONG = "Something went wrong.";
    public static final String SAVED_SUCCESSFULLY = "Saved successfully.";
    public static final String UPDATED_SUCCESSFULLY = "Updated successfully.";
    public static final String USER_BLOCKED_SUCCESSFULLY = "User blocked successfully.";
    public static final String FAILED_TO_BLOCKED_USER = "Fail to blocked user.";
    public static final String USER_UNBLOCKED_SUCCESSFULLY = "User unblocked successfully.";
    public static final String FAILED_TO_UNBLOCKED_USER = "Failed to unblocked user.";
    public static final String LINK_BROKEN = "Link broken to access this route.";
    public static final String DELETED_SUCCESSFULLY = "Deleted successfully.";
    public static final String APPROVED_SUCCESSFULLY = "Approved successfully.";
    public static final String REJECTED_SUCCESSFULLY = "Rejected successfully.";
    public static final String FAILED_TO_SAVE = "Sorry but failed to save.";
    public static final String FAILED_TO_REJECT = "Sorry but failed to reject.";
    public static final String FAILED_TO_APPROVE = "Sorry but failed to approve.";
    public static final String FAILED_TO_SAVE_CHANGES = "Sorry but failed to save changes.";
    public static final String RESET_SUCCESSFULLY = "Reset successfully.";
    public static final String FAILED_TO_UPDATE = "Sorry but failed to update.";
    public static final String FAILED_TO_DELETE = "Sorry but failed to delete.";
    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd mm:hh:ss";
    public static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final String FILE_UPLOAD_BASE_PATH = "E:\\Project\\file-uploads\\";

}
